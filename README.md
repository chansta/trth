# TRTH Python Script.

This repo contains a few python functions to clearn and extract data from Thomson Reuters Tick History (TRTH) data. 

## Files:
    1. Intradaily.py
    2. convert.ipynb
    3. dji_example.csv and dji_example_5mins.csv

### Intradaily.py
    This is the main files for the Python function. Further explanations can be found in their doc strings. The main function is *convert* which allows users to convert tick data into a fixed sampling intervals (in minutes).   

### convert.ipynb
    A simple jupyter notebook to demonstrate how convert works. 

### dji_example.csv and dji_example_5mins.csv
    Example input and output data. 

