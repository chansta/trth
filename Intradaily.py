##################################################################################################
"""
name:				Intradaily.py
author:				Felix Chan
email:				fmfchan@gmail.com
date created:			2019.09.27
description:			Utlity for handling intradaily data from TRTH
"""
##################################################################################################

import sqlite3 as sql
import numpy as np
import pandas as pd
import datetime as dt
import dateutil as du

def datetostring(date,time):
    """
    Combine date and time to form a string. 
    Inputs:
        date: datetime object on date. 
        time: datetime object on time. 
    Output:
        datestring: string. 
    """
    temp = dt.datetime.combine(date,time)
    return temp.strftime('%Y-%m-%dT%H:%M:%S')

def gen_timeslots(starttime, endtime, frequency=5):
    """
    Generate a list of timeslot within a day. 
    Inputs:
        starttime: datetime object. Starting time. 
        endtime: datetime object. Endtime. 
        frequency: int. Number of minutes
    Output:
        timeslots: list of datetime.time object
    """
    
    delta = dt.timedelta(minutes=frequency)
    timeslots = [starttime.time()]
    current = starttime
    while current.time() < endtime.time():
        current = current + delta
        timeslots.append(current.time())
    return timeslots

def convert(input_f, output_f, starttime, endtime, frequency=5, date_pos=2):
    """
    Convert a TRTH dataset in csv into a specific frequency. Datetime format assume to be %Y-%m-%dT%H:%M:%S.SS
    Inputs:
        input_f: string. Input filename. The original datafile in csv. 
        output_f: string. Output filename. 
        starttime: time object. Starting time. 
        endtime: time object. Endtime. 
        frequency: int. Number of minutes
        date_pos: int. The column number that contains the date-time data. Index from 0. 
    Outputs:
        Data will be written to the file specified by output_f. Two additional columns will be added.
        ROWID: The row id of the original rows
        Interval: The date and time of the intervals where the extraction is based on.  
    """
    start = dt.datetime.combine(dt.date(1900,1,1), starttime)
    end = dt.datetime.combine(dt.date(1900,1,1), endtime)
    timeslots = gen_timeslots(start, end, frequency)
    f = open(input_f)
    slot_pos = 0
    for i,line in enumerate(f):
        if i == 0:
            s = ','.join(['ROWID', 'Interval',line])
        elif i == 1:
            tempdatetime = du.parser.isoparse(line.split(',')[date_pos])
            benchdate = tempdatetime.date()
            benchtime = tempdatetime.time()
        else:
            tempdatetime = du.parser.isoparse(line.split(',')[date_pos])
            currentdate = tempdatetime.date()
            currenttime = tempdatetime.time()
            if currentdate > benchdate:
                benchdate = currentdate
                benchtime = currenttime
                slot_pos = 0
            else:
                if (currenttime > timeslots[slot_pos]) and (currenttime < endtime):
                    addstring = datetostring(benchdate,timeslots[slot_pos])
                    slot_pos = slot_pos + 1
                    s = s+','.join([str(i), addstring, lastline])
                benchtime = currenttime
        lastline = line
    f.close()
    f = open(output_f,'w')
    f.write(s)
    f.close()
    

